#include "spring/Framework/Application.h"

namespace Spring
{
	Application::Application()
	{
	}

	Application & Application::getInstance()
	{
		if (_instance == nullptr)
		{
			_instance = new Application();
		}
		else
		{
			return * _instance;
		}
	}

	Application::~Application()
	{
	}

	void Application::setApplicationModel(Spring::IApplicationModel * av_xAppModel)
	{
		mv_pAppModel = av_xAppModel;

		//TODO: cheama metodele pt definirea scenelor, tranzitiilor si a scenei initiale
	}

	void Application::start(const std::string & av_szApplicationTitle, const unsigned ac_nWidth, const unsigned ac_nHeight)
	{
		mv_uxMainWindow->setWindowTitle(QString::fromStdString(av_szApplicationTitle));
		mv_uxMainWindow->setFixedSize(ac_nWidth, ac_nHeight);

		mv_uxMainWindow->show();
	}
}
