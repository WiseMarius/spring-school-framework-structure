#include <map>
#include "..\..\..\include\spring\Framework\SceneMachine.h"

namespace Spring
{
	void CSceneMachine::getActiveScene(const std::string & a_NextSceme)
	{
		if (mv_sCurrentScene != a_NextSceme)
		{
			IScene * currentScene = nullptr;

			if (mv_sCurrentScene != "")
			{
				const auto & it = m_SceneCollection.find(mv_sCurrentScene);
				currentScene = it->second;

				mv_xMainWindow = std::move(currentScene->uGetWindow());
			}
		}
	}

	CSceneMachine::CSceneMachine(std::map<const std::string, Spring::IScene*> m_SceneCollection, std::map<const std::string, const boost::any> m_TransientDataCollection, const std::string mc_sInitialScene, std::unique_ptr<QMainWindow> mv_xMainWindow)
	{
		for (auto & scene : m_SceneCollection)
		{
			QObject::connect(scene.second, SIGNAL(sceneChange(const std::string &)),
				this,
				SLOT(getActiveScene(const std::string &)));
		}
	}
}
