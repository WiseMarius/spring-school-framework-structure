#pragma once
#include <qmainwindow.h>
#include <memory>

#include "spring/Framework/IApplicationModel.h"
#include "spring/Framework/SceneMachine.h"

namespace Spring
{
	typedef std::unique_ptr<QMainWindow> uxQWindow;

	class Application
	{
	public:
		static Application& getInstance();
		~Application();

		void setApplicationModel(Spring::IApplicationModel * av_xAppModel);
		void start(const std::string & av_szApplicationTitle, const unsigned ac_nWidth, const unsigned ac_nHeight);

	private:
		Application();
		static Application * _instance;

		Spring::CSceneMachine * m_SceneMachine;
		Spring::IApplicationModel * mv_pAppModel;
		std::unique_ptr<QMainWindow> mv_uxMainWindow;
	};
}
