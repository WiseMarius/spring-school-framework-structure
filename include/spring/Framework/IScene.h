#pragma once
#include <iostream>
#include <map>
#include <boost/any.hpp>
#include <memory>
#include <qmainwindow.h>


namespace Spring
{
	class IScene : public QObject
	{
		Q_OBJECT

	protected:
		std::map<const std::string, const boost::any> m_TransientDataColletion;
		std::unique_ptr<QMainWindow> m_uMainWindow;
	private:
		const std::string mc_szSceneName;
	signals:
		void sceneChange(const std::string);
	public:
		IScene(const std::string & ac_szSceneName);
		~IScene();

		void createScene();
		void drawScene()
		{
			QMetaObject::connectSlotsByName(m_uMainWindow.get());
		}
		void release();

		void setTransientDAta(std::map<const std::string, const boost::any> & ac_xTransientData);
		void setWindow(std::unique_ptr<QMainWindow> & av_xMainWindow);
		std::string sGetSceneName();

		void show()
		{
			QMainWindow *myWindow = new QMainWindow();
			myWindow->show();
		}

		std::map<const std::string, const boost::any> uGetTransientData();
		std::unique_ptr<QMainWindow> & uGetWindow();
	};

}
