#include "IApplicationModel.h"
#include "IScene.h"

std::string Spring::IApplicationModel::getInitialSceneName()
{
	return m_szInitialScene;
}

Spring::IScene * Spring::IApplicationModel::getInitialScene()
{
	return m_Scene[m_szInitialScene];
}

std::map<const std::string, Spring::IScene*> Spring::IApplicationModel::getScenes()
{
	return m_Scene;
}

std::map<const std::string, const boost::any> Spring::IApplicationModel::getTransientData()
{
	return m_TransientData;
}
