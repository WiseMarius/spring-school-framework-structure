#pragma once
#include <map>
#include "IScene.h"
#include <boost\any.hpp>

namespace Spring
{
	class CSceneMachine : QObject
	{
		Q_OBJECT

	private:
		std::map<const std::string, Spring::IScene *> m_SceneCollection;
		std::map<const std::string, const boost::any> m_TransientDataCollection;

		const std::string mc_sInitialScene;
		std::string mv_sCurrentScene;
		std::unique_ptr<QMainWindow> mv_xMainWindow;

		CSceneMachine();
		void getActiveScene(const std::string & ac_sSceneName);

	private slots:
		void setActiveScene(const std::string);
		void release();

	public:
		CSceneMachine(std::map<const std::string, Spring::IScene *> m_SceneCollection, std::map<const std::string, const boost::any> m_TransientDataCollection, const std::string mc_sInitialScene, std::unique_ptr<QMainWindow> mv_xMainWindow);

		~CSceneMachine();
	};
}