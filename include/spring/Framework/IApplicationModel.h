#pragma once
#include <map>
#include <boost/any.hpp>

#include "IScene.h"

namespace Spring
{
	class IApplicationModel
	{
	public:
		virtual ~IApplicationModel();

		virtual void defineScenes();
		virtual void defineTransientData();
		virtual void defineInitialScene();

		std::string getInitialSceneName();
		IScene* getInitialScene();
		std::map<const std::string, IScene*> getScenes();
		std::map<const std::string, const boost::any> getTransientData();

	private:
		std::map<const std::string, IScene*> m_Scene;
		std::map<const std::string, const boost::any> m_TransientData;
		std::string m_szInitialScene;
	};
}